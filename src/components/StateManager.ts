import { ref, unref, reactive } from 'vue';

interface State {
  path: string,
  editing: boolean
}
type StateArray = Array<State>;

export const StateManager = {
  debug: true,

  updateKey: ref<number>(0),

  message: reactive({
    content: '',
    type: 'info'
  }),

  state: reactive<State>({
    path: '',
    editing: false
  }),

  history: reactive<StateArray>([]),


  incrementUpdateKey: function() {
    this.updateKey = ref(unref(this.updateKey) + 1);
  },

  tell(text: string) {
    this.message.content = text;
    this.message.type = 'info';
  },

  tellError(text: string) {
    this.message.content = text;
    this.message.type = 'error';
  },

  goToPath: function(path: string) {
    if (this.debug) {
      console.log('showing contents of', path);
    }
    let currentState: State = {
      path: this.state.path,
      editing: this.state.editing
    };
    this.history.push(currentState);
    this.state.path = path;
    this.state.editing = false;
    this.incrementUpdateKey();
  },

  goBack: function() {
    if (this.debug) {
      console.log('Going back to contents of',
                  this.history.slice(-1)[0].path || '/');
    }
    if(this.history.length) {
      let state = this.history.pop();
      this.state = state as State;
      this.incrementUpdateKey();
    }
  },

  editFile: function(file: string) {
    if (this.debug) {
      console.log('opening file', file);
    }
    let currentState: State = {
      path: this.state.path,
      editing: this.state.editing
    };
    this.history.push(currentState);
    this.state.path = file;
    this.state.editing = true;
  }
};
