export default {
  install: (app: any, options: Object) => {
    app.config.globalProperties.$translate = function(key: string) {
      return key.split('.').reduce((o: any, i: string) => {
        if (o) return o[i];
      }, options)
    };
    app.provide('i18n', options);
  }
}
