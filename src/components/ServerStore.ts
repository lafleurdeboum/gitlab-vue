import { ref, reactive } from 'vue';
import axios, { AxiosInstance } from 'axios';

type GitlabServer = {
  name: string;
  url: string;
  clientId: string;
  clientSecret: string
}
type ServerList = Array<GitlabServer>;

type Creds = {
  server: GitlabServer;
  project: string;
  token: string;
}

function isCreds(creds: Creds | null ): creds is Creds {
  return (creds as Creds).server !== undefined
         && (creds as Creds).server.name !== undefined;
}

const gitlab: GitlabServer = {
  name: 'gitlab.com',
  url: 'https://gitlab.com',
  clientId: 'e9220cb15973f6736c2f46b934978897e69e495255c603d12aa34c4a011d3282',
  clientSecret: '9ab8d6169e7d038768053a683ca18401dab4e746cec4d5e5d2d2735d92351486',
}

const framagit: GitlabServer = {
  name: 'framagit.org',
  url: 'https://framagit.org',
  clientId: '',
  clientSecret: '',
}

const STORAGE_KEY = 'gitlab-vue';

export const ServerStore = {
  hasCreds: function() {
    return this.creds.server !== undefined && this.creds.server.url !== '';
  },
  hasProject: function() {
    return this.creds.project !== undefined && this.creds.project !== '';
  },
  hasToken: function() {
    return this.creds.token !== undefined && this.creds.token !== '';
  },

  gitlabServers: {
    gitlab: gitlab,
    framagit: framagit
  },

  creds: reactive<Creds>({
    server: reactive<GitlabServer>({
      name: '',
      url: '',
      clientId: '',
      clientSecret: ''
    }),
    project: '',
    token: ''
  }),

  api: axios.create(),

  setServer: function(server: GitlabServer) {
    /* TODO We could append a server creds :
    if(!(server.name in this.gitlabServers)) {
      this.gitlabServers[server.name] = server;
    }
    */
    this.creds.server = server;
    // Write to local storage :
    this.saveCreds(this.creds);
  },

  setProject: function(project: string) {
    this.creds.project = project;
    // Write to local storage :
    this.saveCreds(this.creds);
  },

  loadCreds: function() {
    let payload = localStorage.getItem(STORAGE_KEY);
    let creds = JSON.parse(payload as string);
    if(creds !== null && isCreds(creds)) {
      this.creds = creds;
      if(this.hasCreds() && this.hasToken()) {
        this.setApi();
      }
    } else {
      console.error('saved credentials do not fit');
    }
  },

  saveCreds: function(creds: Creds) {
    this.creds = creds;
    if(this.hasCreds() && this.hasToken()) {
      this.setApi();
    }
    console.log('saving creds:', creds);
    localStorage.setItem(STORAGE_KEY, JSON.stringify(creds));
  },

  setApi: function() {
    this.api = axios.create({
      baseURL: this.creds.server.url + '/api/v4/projects/' 
               + encodeURIComponent(this.creds.project),
      headers: {
        'Authorization': 'Bearer ' + this.creds.token
      }
    });
  },

  setToken: function(token: string) {
    this.creds.token = token;
  }
}
