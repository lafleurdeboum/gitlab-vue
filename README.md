# GitlabVue

Access and modify files on a gitlab project. Uses https connection to connect to
gitlab.


1.1 Build
```
yarn install
```

1.2 Run
```
yarn serve
```
Then follow the printed link.

On the app's page, click settings and fill in :
  - `Addresse de gitlab` to the full url of the gitlab instance to connect to
  - `Nom du projet` in the format `user/project`.
  - `Token d'accès` to an access token generated in the user's gitlab web
    interface


### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
