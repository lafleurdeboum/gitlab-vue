import { createApp } from 'vue';
import App from './App.vue';
import i18nPlugin from './plugins/i18n';

var vueApp = createApp(App);
const i18nStrings = {
  welcome_message: `Bienvenue dans GitlabVue ! Choisissez l'instance Gitlab qui
  héberge vos projets.`,
  welcome_get_token: `Votre fournisseur Gitlab est bien sélectionné, suivez le
  lien suivant pour obtenir une autorisation d'accès à vos projets.`,
  welcome_choose_project: `Vous êtes bien connecté.e à votre instance Gitlab,
  choisissez un projet.`,
  get_auth: 'autoriser GitlabVue à modifier vos project dans',
  server: 'serveur',
  project: 'projet',
  new_page: 'nouvelle page',
  pages: 'pages',
  content: 'contenu',

  connecting: 'connection en cours',
  listing_failed: 'impossible de lister le dossier',
  network_error: 'connection impossible',
  no_auth_error: 'vous n\'êtes pas autorisé.e',
  unknown_url: 'adresse Gitlab invalide',
  unknown_id: 'nom du projet ou token d\'accès invalide',
  unknown_error: 'erreur inconnue',
  file_read_failed: 'impossible de lire le fichier',
  file_saved: 'enregistré',
  file_save_failed: 'impossible d\'enregistrer le fichier',
  file_created: 'créé',
  file_create_failed: 'impossible de créer le fichier',
  file_deleted: 'supprimé',
  file_delete_failed: 'impossible de supprimer le fichier',
  creds_updated: 'identifiants enregistrés',
  creds_removed: 'identifiants oubliés',
}

vueApp.use(i18nPlugin, i18nStrings);

vueApp.mount('#app');
